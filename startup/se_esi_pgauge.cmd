#@field PORTNAME
#@type STRING
#The port name. Should be unique within an IOC.
#
#@field IPADDR
#@type STRING
#IP or hostname of the TCP endpoint.
#
#@field IPPORT
#@type INTEGER
#Port number for the TCP endpoint.
#
#@field PREFIX
#@type STRING
#Prefix for EPICS PVs.


# Set parameters when not using auto deployment
epicsEnvSet(PORTNAME, "PortA")
epicsEnvSet(IPADDR, "10.4.3.145")
epicsEnvSet(IPPORT, "4001")
epicsEnvSet(PREFIX, "Utg-SEPool:SEE-Pmonit-PG01")


# All require need to have version number
require se_esi_pgauge, local
require streamdevice, 2.7.7

#Specifying the TCP endpoint and port name
drvAsynIPPortConfigure("$(PORTNAME)", "$(IPADDR):$(IPPORT)")


#Load your database defining the EPICS records
dbLoadRecords(se_esi_pgauge.template, "P=$(PREFIX), PORT=$(PORTNAME), ADDR=$(IPPORT)")
